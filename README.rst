########################################
Modular Amplifier with separate Feedback
########################################

Overview
========

A simple Two-Channel Audio Amplifier with lead out feedback/sense signal.
This device is designed such that it will fit into a eurorack synthesizer
to be used for monitoring the audio signal. A second use of the amplifier is
in loudpseaker chassis analysis: with signal applied only to one of the inputs,
the other input can be wired with external reference resistor between its output
and feedback such that the current and voltage of chassis can be measured.


Status
======

Evaluation PCB finished and tested sucessfully, schematic & pcb updated
with some bugfixes.

Licensing
=========

CC BY-SA for all files in ``hardware`` subfolder

Copyright
=========

For all files in ``hardware`` folder copyright is
(c) 2022 Andreas Messer <andi@bastelmap.de>.



